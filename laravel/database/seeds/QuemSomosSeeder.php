<?php

use Illuminate\Database\Seeder;

class QuemSomosSeeder extends Seeder
{
    public function run()
    {
        DB::table('quem_somos')->insert([
            'texto' => '',
            'texto_equipe' => '',
            'metodologia' => '',
            'etapa_1_titulo' => '',
            'etapa_1_texto' => '',
            'etapa_2_titulo' => '',
            'etapa_2_texto' => '',
            'etapa_3_titulo' => '',
            'etapa_3_texto' => '',
        ]);
    }
}
