<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'email'       => '@',
            'localizacao' => 'Cidade Jardim - SP',
            'telefone'    => '+55 11 3562·3737'
        ]);
    }
}
