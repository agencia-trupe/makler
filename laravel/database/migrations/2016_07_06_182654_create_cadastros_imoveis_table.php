<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCadastrosImoveisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cadastros_imoveis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('email');
            $table->string('telefone');
            $table->string('tipo');
            $table->string('metragem');
            $table->string('bairro');
            $table->string('endereco');
            $table->string('valor');
            $table->boolean('lido')->default(0);
            $table->timestamps();
        });

        Schema::create('cadastros_imoveis_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cadastro_id')->unsigned();
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('cadastro_id')->references('id')->on('cadastros_imoveis')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cadastros_imoveis_imagens');
        Schema::drop('cadastros_imoveis');
    }
}
