<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuemSomosTable extends Migration
{
    public function up()
    {
        Schema::create('quem_somos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->text('texto_equipe');
            $table->text('metodologia');
            $table->string('etapa_1_titulo');
            $table->text('etapa_1_texto');
            $table->string('etapa_2_titulo');
            $table->text('etapa_2_texto');
            $table->string('etapa_3_titulo');
            $table->text('etapa_3_texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('quem_somos');
    }
}
