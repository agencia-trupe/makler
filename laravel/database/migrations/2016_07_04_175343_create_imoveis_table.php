<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImoveisTable extends Migration
{
    public function up()
    {
        Schema::create('imoveis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('status');
            $table->string('titulo');
            $table->string('slug');
            $table->string('subtitulo');
            $table->string('imagem');
            $table->text('caracteristicas_do_imovel');
            $table->text('caracteristicas_do_condominio');
            $table->text('localizacao');
            $table->text('valores');
            $table->timestamps();
        });

        Schema::create('imoveis_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('imovel_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('imovel_id')->references('id')->on('imoveis')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('imoveis_imagens');
        Schema::drop('imoveis');
    }
}
