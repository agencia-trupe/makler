<?php

return [

    'name'        => 'Makler Realty',
    'title'       => 'Makler Realty &middot; Agência de Imóveis',
    'description' => 'MAKLER REALTY AGÊNCIA DE IMÓVEIS tem como principal intuito conseguir imóveis para seus clientes e não necessariamente clientes para seus imóveis.',
    'keywords'    => 'Imóveis, Apartamentos, salas comerciais, lajes corporativas, locação comercial, lançamentos, novos, usados, compra, alto padrão',
    'share_image' => 'facebook.jpg',
    'analytics'   => 'UA-80997544-1'

];
