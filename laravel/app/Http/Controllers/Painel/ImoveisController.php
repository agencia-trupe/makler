<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ImoveisRequest;
use App\Http\Controllers\Controller;

use App\Models\Imovel;

class ImoveisController extends Controller
{
    public function index()
    {
        $registros = Imovel::ordenados()->get();

        return view('painel.imoveis.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.imoveis.create');
    }

    public function store(ImoveisRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = Imovel::uploadImagem();

            Imovel::create($input);
            return redirect()->route('painel.imoveis.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Imovel $registro)
    {
        return view('painel.imoveis.edit', compact('registro'));
    }

    public function update(ImoveisRequest $request, Imovel $registro)
    {
        try {

            $input = $request->all();
            if (isset($input['imagem'])) $input['imagem'] = Imovel::uploadImagem();

            $registro->update($input);
            return redirect()->route('painel.imoveis.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Imovel $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.imoveis.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
