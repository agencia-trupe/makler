<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\CadastroImovel;

class CadastroImoveisController extends Controller
{
    public function index()
    {
        $cadastros = CadastroImovel::orderBy('id', 'DESC')->paginate(15);

        return view('painel.cadastro-de-imoveis.index', compact('cadastros'));
    }

    public function show(CadastroImovel $cadastro)
    {
        $cadastro->update(['lido' => 1]);

        return view('painel.cadastro-de-imoveis.show', compact('cadastro'));
    }

    public function destroy(CadastroImovel $cadastro)
    {
        try {

            $cadastro->delete();
            return redirect()->route('painel.cadastro-de-imoveis.index')->with('success', 'Cadastro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir cadastro: '.$e->getMessage()]);

        }
    }
}
