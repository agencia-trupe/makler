<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\MessageBag;

use Validator;

use App\Models\CadastroImovel;
use App\Models\Contato;

class CadastroImovelController extends Controller
{
    private $errors;
    private $cadastro;

    public function __construct(Request $request)
    {
        $this->errors = new MessageBag;
        $this->request = $request;
    }

    public function index()
    {
        return view('frontend.cadastro-imovel');
    }

    public function post(Request $request)
    {
        $this->validaCampos();
        $this->validaImagens();

        if (count($this->errors->all()) > 0) {
            return back()->withInput()->withErrors($this->errors);
        }

        $this->salvaCadastro();
        $this->uploadImagens();
        $this->enviaEmail();

       return back()->with(['enviado' => true]);
    }

    public function validaCampos()
    {
        $input = $this->request->except('imagens');

        $rules = [
            'nome'     => 'required',
            'email'    => 'required|email',
            'tipo'     => 'required',
            'metragem' => 'required',
            'bairro'   => 'required',
            'endereco' => 'required',
            'valor'    => 'required',
        ];

        $messages = [
            'nome.required'     => 'Preencha seu nome',
            'email.required'    => 'Preencha seu e-mail',
            'email.email'       => 'Insira um endereço de e-mail válido',
            'tipo.required'     => 'Selecione um tipo de imóvel',
            'metragem.required' => 'Preencha a metragem',
            'bairro.required'   => 'Preencha o bairro',
            'endereco.required' => 'Preencha o endereço',
            'valor.required'    => 'Preencha o valor',
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            foreach($validator->errors()->all() as $erro) {
                $this->errors->add('campos', $erro);
            }
        }
    }

    public function validaImagens()
    {
        if ( ! $this->request->hasFile('imagens')) {
            return $this->errors->add('imagens', 'O campo de imagens é obrigatório');
        }

        $rules = [
            'imagem' => 'max:2000|image'
        ];

        $messages = [
            'max'   => 'O arquivo {{arquivo}} excede o limite de 2MB',
            'image' => 'O arquivo {{arquivo}} não é um arquivo de imagem',
        ];

        foreach($this->request->file('imagens') as $key => $imagem) {
            $validator = Validator::make(['imagem' => $imagem], $rules, $messages);

            if ($validator->fails()) {
                foreach($validator->errors()->all() as $erro) {
                    $this->errors->add('imagem', str_replace('{{arquivo}}', $imagem->getClientOriginalName(), $erro));
                }
            }
        }
    }

    public function salvaCadastro()
    {
        try {
            $this->cadastro = CadastroImovel::create($this->request->except('imagens'));
        } catch (\Exception $e) {
            $this->errors->add('upload', 'Ocorreu um erro ao salvar o cadastro. Tente novamente');
            return back()->withInput()->withErrors($this->errors);
        }
    }

    public function uploadImagens()
    {
        try {
            foreach($this->request->file('imagens') as $imagem) {
                $name = str_slug(pathinfo($imagem->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$imagem->getClientOriginalExtension();
                $path = 'cadastros-de-imoveis/';

                $imagem->move($path, $name);
                $this->cadastro->imagens()->create(['imagem' => $name]);
            }
        } catch (\Exception $e) {
            $this->errors->add('upload', 'Ocorreu um erro ao fazer o upload das imagens. Tente novamente');
            return back()->withInput()->withErrors($this->errors);
        }
    }

    public function enviaEmail()
    {
        $contato  = Contato::first();
        $cadastro = $this->cadastro;

        try {
            if (isset($contato->email)) {
                \Mail::send('emails.cadastro-imovel', compact('cadastro'), function($message) use ($cadastro, $contato)
                {
                    $message->to($contato->email, config('site.name'))
                            ->subject('[CADASTRO DE IMÓVEL] '.config('site.name'))
                            ->replyTo($cadastro->email, $cadastro->nome);
                });
            }
        } catch (\Exception $e) {
            $this->errors->add('upload', 'Ocorreu um erro ao enviar o cadastro. Tente novamente');
            return back()->withInput()->withErrors($this->errors);
        }
    }
}
