<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Models\Imovel;
use App\Models\Depoimento;

class HomeController extends Controller
{
    public function index()
    {
        $banners = Banner::ordenados()->get();
        $imoveis = Imovel::whereStatus('inativo')->ordenados()->take(3)->get();
        $depoimentos = Depoimento::get();

        return view('frontend.home', compact('banners', 'imoveis', 'depoimentos'));
    }
}
