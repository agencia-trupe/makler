<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Imovel;

class ImoveisController extends Controller
{
    public function index()
    {
        $ativos = Imovel::whereStatus('ativo')->ordenados()->get();
        $inativos = Imovel::whereStatus('inativo')->ordenados()->get();

        return view('frontend.imoveis.index', compact('ativos', 'inativos'));
    }

    public function show(Imovel $imovel)
    {
        $outros = Imovel::whereStatus('ativo')->where('id', '!=', $imovel->id)->ordenados()->take(4)->get();

        return view('frontend.imoveis.show', compact('imovel', 'outros'));
    }
}
