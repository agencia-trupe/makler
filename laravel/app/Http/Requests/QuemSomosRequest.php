<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class QuemSomosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto' => 'required',
            'texto_equipe' => 'required',
            'metodologia' => 'required',
            'etapa_1_titulo' => 'required',
            'etapa_1_texto' => 'required',
            'etapa_2_titulo' => 'required',
            'etapa_2_texto' => 'required',
            'etapa_3_titulo' => 'required',
            'etapa_3_texto' => 'required',
        ];
    }
}
