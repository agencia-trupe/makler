<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CadastroImovelRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = $this->instance()->all();

        $rules = [
            'imagens' => 'required'
        ];

        if (isset($request['imagens'])) {
            foreach($request['imagens'] as $key => $file) {
                $rules['imagens.'.$key] = 'image|mimes:jpeg,png,gif';
            }
        }

        return $rules;
    }

    public function messages()
    {
        $request = $this->instance()->all();

        $messages = [
            'files.required' => 'You must upload a file.'
        ];

        if (isset($request['imagens'])) {
            foreach($request['imagens'] as $key => $file) {
                $messages['imagens.'.$key.'.image'] = 'The upload file must be an image.';
                $messages['imagens.'.$key.'.mimes'] = 'The image must be one of the following types: JPEG, PNG, or GIF.';
            }
        }
        return $messages;
    }
}
