<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ImoveisRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'status' => 'required',
            'titulo' => 'required',
            'slug' => '',
            'subtitulo' => 'required',
            'imagem' => 'required|image',
            'caracteristicas_do_imovel' => '',
            'caracteristicas_do_condominio' => '',
            'localizacao' => '',
            'valores' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
