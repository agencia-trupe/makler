<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('quem-somos', 'QuemSomosController@index')->name('quem-somos');
    Route::get('parceiros', 'ParceirosController@index')->name('parceiros');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');
    Route::get('imoveis', 'ImoveisController@index')->name('imoveis');
    Route::get('imoveis/{imovel_slug?}', 'ImoveisController@show')->name('imoveis.show');
    Route::get('cadastre-seu-imovel', 'CadastroImovelController@index')->name('cadastro-imovel');
    Route::post('cadastre-seu-imovel', 'CadastroImovelController@post')->name('cadastro-imovel.post');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        Route::resource('cadastro-de-imoveis', 'CadastroImoveisController');
        Route::resource('imoveis', 'ImoveisController');
        Route::get('imoveis/{imoveis}/imagens/clear', [
            'as'   => 'painel.imoveis.imagens.clear',
            'uses' => 'ImovelImagensController@clear'
        ]);
        Route::resource('imoveis.imagens', 'ImovelImagensController');
        Route::resource('equipe', 'EquipeController');
        Route::resource('quem-somos', 'QuemSomosController', ['only' => ['index', 'update']]);
        Route::resource('parceiros', 'ParceirosController');
        Route::resource('depoimentos', 'DepoimentosController');
        Route::resource('banners', 'BannersController');
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
