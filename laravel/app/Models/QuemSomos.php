<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuemSomos extends Model
{
    protected $table = 'quem_somos';

    protected $guarded = ['id'];
}
