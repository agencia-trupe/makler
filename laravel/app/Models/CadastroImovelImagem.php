<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CadastroImovelImagem extends Model
{
    protected $table = 'cadastros_imoveis_imagens';

    protected $guarded = ['id'];
}
