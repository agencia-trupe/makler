<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Imovel extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'imoveis';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\ImovelImagem', 'imovel_id')->ordenados();
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            'width'  => 295,
            'height' => 195,
            'path'   => 'assets/img/imoveis/thumbs/'
        ]);
    }
}
