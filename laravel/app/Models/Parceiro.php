<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\CropImage;

class Parceiro extends Model
{
    protected $table = 'parceiros';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            'width'  => 200,
            'height' => 155,
            'parceiro' => true,
            'path'   => 'assets/img/parceiros/'
        ]);
    }
}
