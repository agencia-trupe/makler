<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CadastroImovel extends Model
{
    protected $table = 'cadastros_imoveis';

    protected $guarded = ['id'];

    public function getCreatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }

    public function scopeNaoLidos($query)
    {
        return $query->where('lido', '!=', 1);
    }

    public function countNaoLidos()
    {
        return $this->naoLidos()->count();
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\CadastroImovelImagem', 'cadastro_id');
    }

    public static $tiposDeImoveis = [
        'casa'               => 'Casa',
        'casa-de-vila'       => 'Casa de vila',
        'casa-em-condominio' => 'Casa em condomínio',
        'apartamento'        => 'Apartamento',
        'cobertura'          => 'Cobertura',
        'imovel-comercial'   => 'Imóvel comercial',
    ];
}
