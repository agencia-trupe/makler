@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="banners">
            @foreach($banners as $banner)
            <div class="banner" style="background-image:url('{{ asset('assets/img/banners/'.$banner->imagem) }}')" data-frase="{{ $banner->frase }}"></div>
            @endforeach
            <div class="banner-box">
                <p>
                    @if($banners->first())
                    {{ $banners->first()->frase }}
                    @endif
                </p>
            </div>
            <a href="#" class="banner-trigger"></a>
        </div>

        <div class="comercializacoes">
            <div class="center">
                <h2>CONFIRA ALGUMAS COMERCIALIZAÇÕES RECENTES</h2>
                @foreach($imoveis as $imovel)
                <a href="{{ route('imoveis') }}">
                    <img src="{{ asset('assets/img/imoveis/thumbs/'.$imovel->imagem) }}" alt="">
                    <div class="overlay">
                        <span>{{ $imovel->titulo }}</span>
                    </div>
                </a>
                @endforeach
            </div>
        </div>

        <div class="chamada-cadastro">
            <div class="center">
                <h2>CADASTRE SEU IMÓVEL CONOSCO</h2>
                <p>Nosso banco de imóveis é constantemente atualizado para atender a cada novo cliente.</p>

                <a href="{{ route('cadastro-imovel') }}"></a>
            </div>
        </div>

        @if($depoimentos)
        <div class="depoimentos">
            @foreach($depoimentos as $depoimento)
            <div class="depoimento">
                <div class="center">
                    <p>{!! $depoimento->frase !!}</p>
                </div>
            </div>
            @endforeach
        </div>
        @endif
    </div>

@endsection
