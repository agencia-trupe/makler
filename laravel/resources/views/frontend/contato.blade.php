@extends('frontend.common.template')

@section('content')

    <div class="contato">
        <div class="abertura">
            <div class="center">
                <div class="abertura__texto form-wrapper">
                    <form action="" id="form-contato" method="POST">
                        <input type="text" name="nome" id="nome" placeholder="NOME" required>
                        <input type="email" name="email" id="email" placeholder="E-MAIL" required>
                        <input type="text" name="telefone" id="telefone" placeholder="TELEFONE">
                        <textarea name="mensagem" id="mensagem" placeholder="MENSAGEM" required></textarea>
                        <input type="submit" value="ENVIAR">
                        <div id="form-contato-response"></div>
                    </form>
                </div>

                <div class="abertura__texto">
                    <p class="telefone">{{ $contato->telefone }}</p>
                    <p>{{ $contato->localizacao }}</p>
                    <p>
                        Makler Realty Agência de Imóveis<br>
                        CRECI 28518J
                    </p>
                </div>
            </div>
        </div>
    </div>

@endsection
