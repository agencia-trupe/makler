@extends('frontend.common.template')

@section('content')

    <div class="parceiros">
        <div class="abertura">
            <div class="center">
                <div class="abertura__imagem"></div>
                <div class="abertura__texto">
                    <h2>PARCEIROS</h2>
                </div>
            </div>
        </div>

        <div class="parceiros__thumbs">
            <div class="center">
                @foreach($parceiros as $parceiro)
                <img src="{{ asset('assets/img/parceiros/'.$parceiro->imagem) }}" alt="{{ $parceiro->nome }}">
                @endforeach
            </div>
        </div>
    </div>

@endsection
