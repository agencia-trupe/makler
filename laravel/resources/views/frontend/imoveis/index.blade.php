@extends('frontend.common.template')

@section('content')

    <div class="imoveis">
        <div class="abertura">
            <div class="center">
                <div class="abertura__imagem"></div>

                <div class="abertura__texto">
                    <p>Selecionamos aqui uma pequena amostra de nosso portfólio em regiões nobres de São Paulo.</p>

                    <p>
                        Trabalhamos com imóveis novos, em obra, prontos e usados.<br>
                        Residencial e Comercial<br>Venda ou Locação Comercial
                    </p>
                </div>
            </div>
        </div>

        <div class="imoveis-thumbs ativos">
            <div class="center">
                <h2>IMÓVEIS PARA COMERCIALIZAÇÃO</h2>
                <div>
                    @foreach($ativos as $imovel)
                    <a href="{{ route('imoveis.show', $imovel->slug) }}" class="thumb">
                        <div class="imagem"><img src="{{ asset('assets/img/imoveis/thumbs/'.$imovel->imagem) }}" alt=""></div>
                        <p>
                            {{ $imovel->titulo }}
                            <span>{{ $imovel->subtitulo }}</span>
                        </p>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="imoveis-thumbs">
            <div class="center">
                <h2>IMÓVEIS RECENTEMENTE COMERCIALIZADOS</h2>
                <div>
                    @foreach($inativos as $imovel)
                    <div class="thumb">
                        <div class="imagem"><img src="{{ asset('assets/img/imoveis/thumbs/'.$imovel->imagem) }}" alt=""></div>
                        <p>
                            {{ $imovel->titulo }}
                            <span>{{ $imovel->subtitulo }}</span>
                        </p>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
