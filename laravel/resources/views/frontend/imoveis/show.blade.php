@extends('frontend.common.template')

@section('content')

    <div class="imoveis">
        <div class="abertura">
            <div class="center">
                <div class="abertura__imagem"></div>

                <div class="abertura__texto">
                    <p>Selecionamos aqui uma pequena amostra de nosso portfólio em regiões nobres de São Paulo.</p>

                    <p>
                        Trabalhamos com imóveis novos, em obra, prontos e usados.<br>
                        Residencial e Comercial<br>Venda ou Locação Comercial
                    </p>
                </div>
            </div>
        </div>

        <div class="imoveis-show">
            <div class="center">
                <h2>IMÓVEIS PARA COMERCIALIZAÇÃO</h2>

                <div class="imagens">
                    <div class="galeria">
                        @foreach($imovel->imagens as $imagem)
                        <div class="slide" style="background-image: url('{{ asset('assets/img/imoveis/imagens/'.$imagem->imagem) }}');"></div>
                        @endforeach

                        <a href="#" class="galeria-control cycle-prev">
                            <div></div>
                        </a>
                        <a href="#" class="galeria-control cycle-next">
                            <div></div>
                        </a>
                    </div>

                    <div class="galeria-thumbs" id="pager">
                        @foreach($imovel->imagens as $thumb)
                            <a href="#">
                                <img src="{{ asset('assets/img/imoveis/imagens/thumbs/'.$thumb->imagem) }}" alt="">
                                <div class="overlay"></div>
                            </a>
                        @endforeach
                    </div>
                </div>

                <div class="informacoes">
                    <h3>
                        {{ $imovel->titulo }}
                        <span>{{ $imovel->subtitulo }}</span>
                    </h3>

                    @if($imovel->caracteristicas_do_imovel)
                    <a href="#" class="info-trigger">CARACTERÍSTICAS DO IMÓVEL</a>
                    <div class="info-content">
                        {!! $imovel->caracteristicas_do_imovel !!}
                    </div>
                    @endif

                    @if($imovel->caracteristicas_do_condominio)
                    <a href="#" class="info-trigger">CARACTERÍSTICAS DO CONDOMÍNIO</a>
                    <div class="info-content">
                        {!! $imovel->caracteristicas_do_condominio !!}
                    </div>
                    @endif

                    @if($imovel->localizacao)
                    <a href="#" class="info-trigger">LOCALIZAÇÃO</a>
                    <div class="info-content">
                        {!! $imovel->localizacao !!}
                    </div>
                    @endif

                    @if($imovel->valores)
                    <a href="#" class="info-trigger">VALORES | CONDIÇÕES DE PAGAMENTO</a>
                    <div class="info-content">
                        {!! $imovel->valores !!}
                    </div>
                    @endif
                </div>
            </div>
        </div>

        @if(count($outros) > 0)
        <div class="outros-imoveis imoveis-thumbs">
            <div class="center">
                <h2>MAIS IMÓVEIS PARA COMERCIALIZAÇÃO</h2>
                <div>
                    @foreach($outros as $imovel)
                    <a href="{{ route('imoveis.show', $imovel->slug) }}" class="thumb">
                        <div class="imagem"><img src="{{ asset('assets/img/imoveis/thumbs/'.$imovel->imagem) }}" alt=""></div>
                        <p>
                            {{ $imovel->titulo }}
                            <span>{{ $imovel->subtitulo }}</span>
                        </p>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
        @endif
    </div>


@endsection
