    <footer>
        <div class="center wrapper">
            <div class="col col-links">
                <a href="{{ route('home') }}">&raquo; home</a>
                <a href="{{ route('quem-somos') }}">&raquo; quem somos</a>
                <a href="{{ route('imoveis') }}">&raquo; imóveis</a>
            </div>
            <div class="col col-links">
                <a href="{{ route('cadastro-imovel') }}">&raquo; cadastre seu imóvel</a>
                <a href="{{ route('parceiros') }}">&raquo; parceiros</a>
                <a href="{{ route('contato') }}">&raquo; contato</a>
            </div>
            <div class="col col-marca">
                <img src="{{ asset('assets/img/layout/makler-footer.png') }}" alt="">
            </div>
            <div class="col col-info">
                <p class="telefone">{{ $contato->telefone }}</p>
                <p class="localizacao">{{ $contato->localizacao }}</p>
                <p>
                    Makler Realty Agência de Imóveis<br>
                    CRECI: 28518J
                </p>
            </div>
        </div>

        <div class="copyright">
            <div class="center">
                <p>
                    © {{ date('Y') }} {{ config('site.name') }} - Todos os direitos reservados.
                    <span>|</span>
                    <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                    <a href="http://www.trupe.net" target="_blank" class="logo-trupe">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
