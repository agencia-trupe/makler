    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="header-logo">{{ config('site.name') }}</a>
            <nav id="header-nav">
                <a href="{{ route('home') }}" class="{{ Tools::isActive('home') }}">home</a>
                <a href="{{ route('quem-somos') }}"  class="{{ Tools::isActive('quem-somos') }}">quem somos</a>
                <a href="{{ route('imoveis') }}"  class="{{ Tools::isActive('imoveis*') }}">imóveis</a>
                <a href="{{ route('cadastro-imovel') }}"  class="{{ Tools::isActive('cadastro-imovel') }}">cadastre seu imóvel</a>
                <a href="{{ route('parceiros') }}"  class="{{ Tools::isActive('parceiros') }}">parceiros</a>
                <a href="{{ route('contato') }}"  class="{{ Tools::isActive('contato') }}">contato</a>

                <span class="social-wrapper">
                @if($contato->facebook)
                <a href="{{ $contato->facebook }}" class="social facebook">facebook</a>
                @endif
                @if($contato->instagram)
                <a href="{{ $contato->instagram }}" class="social instagram">instagram</a>
                @endif
                @if($contato->linkedin)
                <a href="{{ $contato->linkedin }}" class="social linkedin">linkedin</a>
                @endif
                </span>
            </nav>

            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>
    </header>
