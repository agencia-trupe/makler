@extends('frontend.common.template')

@section('content')

    <div class="abertura">
        <div class="center">
            <div class="abertura__imagem">
                <img src="{{ asset('assets/img/layout/img-quemsomos.png') }}" alt="">
            </div>

            <div class="abertura__texto">
                {!! $quemSomos->texto !!}
            </div>
        </div>
    </div>

    <div class="equipe">
        <div class="center">
            <h2>Equipe</h2>
            {!! $quemSomos->texto_equipe !!}

            <div class="equipe__membros">
                @foreach($equipe as $membro)
                <div class="membro">
                    <h4>{{ $membro->cargo }}</h4>
                    <h3>{{ $membro->nome }}</h3>
                    <p>{{ $membro->texto }}</p>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="metodologia">
        <div class="center">
            <h2>Nossa metodologia</h2>
            <p>{{ $quemSomos->metodologia }}</p>

            <div class="metodologia__etapas">
                @for($i = 1; $i <= 3; $i++)
                <div class="etapa etapa-{{ $i }}">
                    <div class="imagem"></div>
                    <h3>
                        <span>etapa</span>
                        {{ $quemSomos->{'etapa_'.$i.'_titulo'} }}
                    </h3>
                    <p>{{ $quemSomos->{'etapa_'.$i.'_texto'} }}</p>
                </div>
                @endfor
            </div>
        </div>
    </div>

@endsection
