@extends('frontend.common.template')

@section('content')

    <div class="cadastro-imovel">
        <div class="abertura">
            <div class="center">
                <div class="abertura__imagem">
                    <img src="{{ asset('assets/img/layout/img-cadastre.png') }}" alt="">
                </div>

                <div class="abertura__texto">
                    <p>Cadastre seu imóvel conosco.</p>

                    <h2>CADASTRE SEU IMÓVEL</h2>

                    @if(count($errors) > 0)
                        <ul class="erros">
                            @foreach($errors->all() as $e)
                            <li>{{ $e }}</li>
                            @endforeach
                        </ul>
                    @elseif(session('enviado'))
                        <div class="sucesso">Cadastro enviado com sucesso!</div>
                    @endif

                    <form action="{{ route('cadastro-imovel.post') }}" id="form-cadastro" enctype="multipart/form-data" method="POST" accept-charset="UTF-8">
                        {!! csrf_field() !!}
                        <input type="text" name="nome" placeholder="NOME" id="nome" value="{{ old('nome') }}" required>
                        <input type="email" name="email" placeholder="E-MAIL" id="email" value="{{ old('email') }}" required>
                        <input type="text" name="telefone" placeholder="TELEFONE" id="telefone" value="{{ old('telefone') }}">
                        <select name="tipo" id="tipo"" required>
                            <option value="" @if(old('tipo') == '') selected @endif>
                                TIPO DO IMÓVEL
                            </option>
                            @foreach(\App\Models\CadastroImovel::$tiposDeImoveis as $id => $tipo)
                            <option value="{{ $id }}" @if(old('tipo') == $id) selected @endif>
                                {{ $tipo }}
                            </option>
                            @endforeach
                        </select>
                        <input type="text" name="metragem" placeholder="METRAGEM" id="metragem" value="{{ old('metragem') }}" required>
                        <input type="text" name="bairro" placeholder="BAIRRO" id="bairro" value="{{ old('bairro') }}" required>
                        <input type="text" name="endereco" placeholder="ENDEREÇO" id="endereco" value="{{ old('endereco') }}" required>
                        <input type="text" name="valor" placeholder="VALOR" id="valor" value="{{ old('valor') }}" required>

                        <h3>ADICIONE IMAGENS</h3>
                        <label id="imagens-input">
                            <input type="file" name="imagens[]" id="imagens" accept="image/*" multiple required>
                            <p>Clique aqui ou arraste imagens para esse local</p>
                        </label>

                        <input type="submit" value="CADASTRAR" id="btn-cadastro">
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
