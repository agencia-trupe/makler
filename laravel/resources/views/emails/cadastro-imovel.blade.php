<!DOCTYPE html>
<html>
<head>
    <title>[CADASTRO DE IMÓVEL] {{ config('site.name') }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $cadastro->nome }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $cadastro->email }}</span><br>
@if($cadastro->telefone)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $cadastro->telefone }}</span><br>
@endif
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Tipo:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ \App\Models\CadastroImovel::$tiposDeImoveis[$cadastro->tipo] }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Metragem:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $cadastro->metragem }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Bairro:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $cadastro->bairro }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Endereço:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $cadastro->endereco }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Valor:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $cadastro->valor }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Imagens:</span><br>
    <ul>
        @foreach($cadastro->imagens as $imagem)
        <li>
            <a href="{{ asset('cadastros-de-imoveis/'.$imagem->imagem) }}">
                {{ $imagem->imagem }}
            </a>
        </li>
        @endforeach
    </ul>
</body>
</html>
