@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'quemSomos']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_equipe', 'Texto Equipe') !!}
    {!! Form::textarea('texto_equipe', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('metodologia', 'Metodologia') !!}
    {!! Form::text('metodologia', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('etapa_1_titulo', 'Etapa 1 Título') !!}
            {!! Form::text('etapa_1_titulo', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('etapa_1_texto', 'Etapa 1 Texto') !!}
            {!! Form::textarea('etapa_1_texto', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('etapa_2_titulo', 'Etapa 2 Título') !!}
            {!! Form::text('etapa_2_titulo', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('etapa_2_texto', 'Etapa 2 Texto') !!}
            {!! Form::textarea('etapa_2_texto', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('etapa_3_titulo', 'Etapa 3 Título') !!}
            {!! Form::text('etapa_3_titulo', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('etapa_3_texto', 'Etapa 3 Texto') !!}
            {!! Form::textarea('etapa_3_texto', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
