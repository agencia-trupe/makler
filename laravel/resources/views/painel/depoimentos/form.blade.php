@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('frase', 'Frase') !!}
    {!! Form::textarea('frase', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.depoimentos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
