<ul class="nav navbar-nav">
    <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banners</a>
    </li>
    <li @if(str_is('painel.depoimentos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.depoimentos.index') }}">Depoimentos</a>
    </li>
    <li @if(str_is('painel.quem-somos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.quem-somos.index') }}">Quem Somos</a>
    </li>
	<li @if(str_is('painel.equipe*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.equipe.index') }}">Equipe</a>
	</li>
    <li @if(str_is('painel.imoveis*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.imoveis.index') }}">Imóveis</a>
    </li>
    <li @if(str_is('painel.cadastro-de-imoveis*', Route::currentRouteName())) class="active" @endif">
        <a href="{{ route('painel.cadastro-de-imoveis.index') }}">
            Cadastro de Imóveis
            @if($cadastrosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $cadastrosNaoLidos }}</span>
            @endif
        </a>
    </li>
	<li @if(str_is('painel.parceiros*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.parceiros.index') }}">Parceiros</a>
	</li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
</ul>
