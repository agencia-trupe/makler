@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Imóveis /</small> Editar Imóvel</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.imoveis.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.imoveis.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
