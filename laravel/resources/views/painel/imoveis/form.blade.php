@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('status', 'Status') !!}
    {!! Form::select('status', ['ativo' => 'Para comercialização', 'inativo' => 'Comercializado'], null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('subtitulo', 'Subtítulo') !!}
    {!! Form::text('subtitulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem de Capa') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/imoveis/thumbs/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('caracteristicas_do_imovel', 'Características do Imóvel') !!}
    {!! Form::textarea('caracteristicas_do_imovel', null, ['class' => 'form-control ckeditor', 'data-editor' => 'imovel']) !!}
</div>

<div class="form-group">
    {!! Form::label('caracteristicas_do_condominio', 'Características do Condomínio') !!}
    {!! Form::textarea('caracteristicas_do_condominio', null, ['class' => 'form-control ckeditor', 'data-editor' => 'imovel']) !!}
</div>

<div class="form-group">
    {!! Form::label('localizacao', 'Localização') !!}
    {!! Form::textarea('localizacao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'imovel']) !!}
</div>

<div class="form-group">
    {!! Form::label('valores', 'Valores') !!}
    {!! Form::textarea('valores', null, ['class' => 'form-control ckeditor', 'data-editor' => 'imovel']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.imoveis.index') }}" class="btn btn-default btn-voltar">Voltar</a>
