@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Imóveis /</small> Adicionar Imóvel</h2>
    </legend>

    {!! Form::open(['route' => 'painel.imoveis.store', 'files' => true]) !!}

        @include('painel.imoveis.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
