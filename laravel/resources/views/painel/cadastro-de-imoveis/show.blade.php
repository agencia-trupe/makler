@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Cadastro de Imóveis</h2>
    </legend>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $cadastro->created_at }}</div>
    </div>

    <div class="form-group">
        <label>Nome</label>
        <div class="well">{{ $cadastro->nome }}</div>
    </div>

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">{{ $cadastro->email }}</div>
    </div>

@if($cadastro->telefone)
    <div class="form-group">
        <label>Telefone</label>
        <div class="well">{{ $cadastro->telefone }}</div>
    </div>
@endif

    <div class="form-group">
        <label>Tipo de Imóvel</label>
        <div class="well">{{ \App\Models\CadastroImovel::$tiposDeImoveis[$cadastro->tipo] }}</div>
    </div>

    <div class="form-group">
        <label>Metragem</label>
        <div class="well">{{ $cadastro->metragem }}</div>
    </div>

    <div class="form-group">
        <label>Bairro</label>
        <div class="well">{{ $cadastro->bairro }}</div>
    </div>

    <div class="form-group">
        <label>Endereço</label>
        <div class="well">{{ $cadastro->endereco }}</div>
    </div>

    <div class="form-group">
        <label>Valor</label>
        <div class="well">{{ $cadastro->valor }}</div>
    </div>

    <label>Imagens</label>
    <div class="clearfix">
    @foreach($cadastro->imagens as $imagem)
        <img src="{{ asset('cadastros-de-imoveis/'.$imagem->imagem) }}" alt="" style="max-width:100%;display:block;float:left;margin:5px 15px 15px 0;padding:10px;background:#fff;border-radius:4px;box-shadow:0 0 8px rgba(0, 0, 0, .2)">
    @endforeach
    </div>

    <a href="{{ route('painel.cadastro-de-imoveis.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
