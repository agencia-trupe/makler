@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>Cadastro de Imóveis</h2>
    </legend>

    @if(!count($cadastros))
    <div class="alert alert-warning" role="alert">Nenhum cadastro recebido.</div>
    @else
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Data</th>
                <th>Nome</th>
                <th>E-mail</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($cadastros as $cadastro)

            <tr class="tr-row @if(!$cadastro->lido) warning @endif">
                <td>{{ $cadastro->created_at }}</td>
                <td>{{ $cadastro->nome }}</td>
                <td>{{ $cadastro->email }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.cadastro-de-imoveis.destroy', $cadastro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.cadastro-de-imoveis.show', $cadastro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-align-left" style="margin-right:10px;"></span>Ler mensagem
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    {!! $cadastros->render() !!}
    @endif

@stop
