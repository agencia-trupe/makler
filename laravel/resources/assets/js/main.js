(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.mobileToggle = function() {
        var $handle = $('#mobile-toggle'),
            $nav    = $('#header-nav');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });
    };

    App.bannersHome = function() {
        $.extend($.easing,
        {
            def: 'easeOutQuad',
            easeOutQuad: function (x, t, b, c, d) {
                return -c *(t/=d)*(t-2) + b;
            }
        });

        $.fn.cycle.transitions.scrollVert = {
            before: function(opts, curr, next, fwd) {
                opts.API.stackSlides(opts, curr, next, fwd);

                var height = opts.container.css('overflow','hidden').height();

                opts.cssBefore = {
                    top: fwd ? height : -height,
                    left: 0,
                    opacity: 1,
                    display: 'block',
                    visibility: 'visible'
                };

                opts.animIn = { top: 0 };
                opts.animOut = { top: fwd ? -height : height };

                var frase = $(next).data('frase');
                $('.banner-box p').text(frase);
            }
        };

        var transicaoBanner = function() {
            $('.banner-box').addClass('transition');

            setTimeout(function() {
                $('.banners').cycle('next');
                setTimeout(function() {
                    $('.banner-box').removeClass('transition');
                }, 450);
            }, 400);
        };

        $('.banners').cycle({
            slides: '>.banner',
            fx: 'scrollVert',
            easing: 'easeOutQuad',
            timeout: 0
        });

        $('.banner-trigger').click(function(event) {
            event.preventDefault();
            clearInterval(intervaloSlides);
            transicaoBanner();
        });

        var intervaloSlides = setInterval(function() {
            transicaoBanner();
        }, 4000);
    };

    App.depoimentosHome = function() {
        $('.depoimentos').cycle({
            slides: '>.depoimento',
            autoHeight: 'container',
            timeout: 5000,
            pauseOnHover: true
        });
    };

    App.galeriaImovel = function() {
        $('.galeria').cycle({
            slides: '>.slide',
            fx: 'scrollHorz',
            pager: '#pager',
            timeout: 0,
            pagerTemplate: ''
        });
    };

    App.informacoesImovel = function() {
        $('.info-trigger').click(function(event) {
            event.preventDefault();

            if ($(this).hasClass('active')) return;

            $('.info-trigger').removeClass('active');
            $('.info-content').slideUp();

            $(this).addClass('active').next().slideDown();
        });
    };

    App.envioCadastroImovel = function() {
        var defaultLabel = $('#imagens-input p').text();

        $('#imagens').change(function(e) {
            var fileName = '';

            if (this.files && this.files.length > 1) {
                fileName = this.files.length + ' arquivos selecionados';
            } else {
                fileName = e.target.value.split('\\').pop();
            }

            if (fileName) {
                $('#imagens-input').addClass('active').find('p').text(fileName);
            } else {
                $('#imagens-input').removeClass('active').find('p').text(defaultLabel);
            }
        });
    };

    App.envioContato = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-contato-response');

        $response.fadeOut('fast');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                telefone: $('#telefone').val(),
                mensagem: $('#mensagem').val(),
            },
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
            },
            error: function(data) {
                if (data.responseJSON) {
                    var field = Object.keys(data.responseJSON)[0];
                    var error = data.responseJSON[field];
                    $response.fadeOut().text(error).fadeIn('slow');
                    $form.find('#' + field).focus();
                }
            },
            dataType: 'json'
        });
    };

    App.init = function() {
        this.mobileToggle();
        this.bannersHome();
        this.depoimentosHome();
        this.galeriaImovel();
        this.informacoesImovel();
        this.envioCadastroImovel();
        $('#form-contato').on('submit', this.envioContato);
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
