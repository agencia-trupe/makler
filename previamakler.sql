-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: previamakler.mysql.dbaas.com.br
-- Generation Time: 19-Jul-2016 às 17:38
-- Versão do servidor: 5.6.30-76.3-log
-- PHP Version: 5.6.22-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `previamakler`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `frase` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `frase`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 1, 'IMÓVEIS PARA CLIENTES ,  NÃO CLIENTES PARA IMÓVEIS', 'img-banner1_20160708214419.png', '2016-07-09 00:44:20', '2016-07-14 16:44:58'),
(2, 2, 'LAPIDAMOS O SEU SONHO TRANSFORMANDO-O EM REALIDADE', 'img-banner2_20160708214511.png', '2016-07-09 00:45:12', '2016-07-09 00:45:12');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastros_imoveis`
--

CREATE TABLE `cadastros_imoveis` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `metragem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bairro` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastros_imoveis_imagens`
--

CREATE TABLE `cadastros_imoveis_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `cadastro_id` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `localizacao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linkedin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `telefone`, `localizacao`, `facebook`, `instagram`, `linkedin`, `created_at`, `updated_at`) VALUES
(1, 'contato@maklerrealty.com.br', '+55 11 3562·3737', 'Cidade Jardim - SP', '#', '#', 'Maklér ', NULL, '2016-07-14 18:16:26');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `depoimentos`
--

CREATE TABLE `depoimentos` (
  `id` int(10) UNSIGNED NOT NULL,
  `frase` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `depoimentos`
--

INSERT INTO `depoimentos` (`id`, `frase`, `created_at`, `updated_at`) VALUES
(1, '<p>&quot;A aten&ccedil;&atilde;o&nbsp;e o conhecimento sobre o im&oacute;vel,a localiza&ccedil;&atilde;o e a experiencia com os documentos do im&oacute;vel que adquirimos com o Makl&eacute;r nos deu muita seguran&ccedil;a e alegria.&quot;</p>\r\n', '2016-07-09 00:46:05', '2016-07-14 17:10:47'),
(2, '<p>&nbsp;&quot;Que continue atendendo sempre bem os clientes, buscando o que eles necessitam, com aten&ccedil;&atilde;o e dedica&ccedil;&atilde;o.&quot;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2016-07-14 16:07:21', '2016-07-14 17:09:12'),
(3, '<p>&quot;Conheci o Alexandre &#39;Makl&eacute;r&quot; em um grupo de corrida e na &eacute;poca, j&aacute; tinha comprado um apto na Sa&uacute;de. Depois de uns 6 meses, decidi vender o apto da Sa&uacute;de e comprar mais perto do meu trabalho. Me mostrou e visitamos alguns apartamentos no Campo Belo, Congonhas e Brooklin. Sempre foi bastante paciente, atencioso e &aacute;gil para tirar minhas d&uacute;vidas. Nesse meio tempo, conheceu o meu pai e tamb&eacute;m fechou neg&oacute;cios com ele no interior de SP. O que mais achei legal &eacute; que ele n&atilde;o recomenda, sem meias palavras, im&oacute;veis de empresas que ele n&atilde;o julga ser de alta qualidade. Concluindo, hoje moro no apto no Brooklin gra&ccedil;as &agrave; indica&ccedil;&atilde;o do Alexandre &quot;Makl&eacute;r&quot;.&quot;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2016-07-14 17:04:35', '2016-07-14 17:04:35'),
(4, '<p>- &quot;os servi&ccedil;os prestados pelo Makl&eacute;r &nbsp;sempre teve algo mais&quot; .&nbsp;</p>\r\n\r\n<p>&quot;Alguns profissionais t&ecirc;m o p&eacute;ssimo h&aacute;bito de n&atilde;o equiparar as expectativas de seus clientes com o que se pode fazer por eles no momento da contrata&ccedil;&atilde;o.&quot;&nbsp;</p>\r\n\r\n<p>&quot;Desta forma n&atilde;o atingem a satisfa&ccedil;&atilde;o de seus clientes&nbsp;e acabam deixando a desejar no relacionamento. O Makl&eacute;r &eacute; um profissional transparente e essa atitude ajuda a estabelecer uma rela&ccedil;&atilde;o de confian&ccedil;a, isso &eacute; muito importante ainda mais nas rela&ccedil;&otilde;es que envolve&nbsp;grandes somas, portanto minha experi&ecirc;ncia com o Makl&eacute;r tem aliado nossas expectativas de satisfa&ccedil;&atilde;o&quot;.</p>\r\n', '2016-07-18 17:06:50', '2016-07-18 18:30:53');

-- --------------------------------------------------------

--
-- Estrutura da tabela `equipe`
--

CREATE TABLE `equipe` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `cargo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `equipe`
--

INSERT INTO `equipe` (`id`, `ordem`, `cargo`, `nome`, `texto`, `created_at`, `updated_at`) VALUES
(1, 1, 'Fundador', 'Alexandre Pereira "MAKLÉR"', 'Alexandre Pereira “MAKLÉR”, Bacharel em administração de empresa pela FMU, Técnico em Transações Imobiliárias CRECI-SP: 120890, Avaliador de imóveis CNAI 13302, atua no mercado há 5 anos sempre se destacando com vendas (residenciais e comerciais) e locação comercial de imóveis.', '2016-07-09 00:53:28', '2016-07-16 00:06:22'),
(2, 2, 'Jurídico', 'Dr. Tadeu Frederico Andrade', 'Dr. Tadeu Frederico Andrade Advogado(OAB-SP 31444), atuando principalmente na área do Direito Imobiliário (atendendo no consultivo e no contencioso).\r\nPerito Avaliador de Imóveis, atuando junto a particulares na elaboração de Laudos de Avaliação de Imóveis e também como Assistente Técnico em processos judiciais que envolvam discussões sobre valores patrimoniais (venda ou locação).CRECI-SP: 81866 CNAI: 7090', '2016-07-09 00:53:48', '2016-07-16 00:06:02'),
(3, 3, 'Técnico', 'Luciana Krettlis Gonçalves', 'Luciana Krettlis Gonçalves Arquiteta(CAU-SP: A30660-6) , atuando na elaboração de Laudos de Avaliação de Imóveis junto a instituições bancárias e particulares, como também atua em Reformas e/ou Construção de imóveis em geral.', '2016-07-09 00:54:12', '2016-07-18 18:29:31');

-- --------------------------------------------------------

--
-- Estrutura da tabela `imoveis`
--

CREATE TABLE `imoveis` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caracteristicas_do_imovel` text COLLATE utf8_unicode_ci NOT NULL,
  `caracteristicas_do_condominio` text COLLATE utf8_unicode_ci NOT NULL,
  `localizacao` text COLLATE utf8_unicode_ci NOT NULL,
  `valores` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `imoveis`
--

INSERT INTO `imoveis` (`id`, `ordem`, `status`, `titulo`, `slug`, `subtitulo`, `imagem`, `caracteristicas_do_imovel`, `caracteristicas_do_condominio`, `localizacao`, `valores`, `created_at`, `updated_at`) VALUES
(1, 6, 'ativo', 'Colina São Francisco - São Paulo', 'colina-sao-francisco-sao-paulo', 'Cobertura Duplex Única', 'dsc-0002_20160713181138.jpg', '<p>Cobertura Duplex&nbsp;</p>\r\n\r\n<p>269 m2 3 Su&iacute;tes 3 VGS</p>\r\n\r\n<p>Piso Inferior :</p>\r\n\r\n<p>3 Su&iacute;tes Sala com 2 ambientes + Sala de jantar. Cozinha e &aacute;rea de servi&ccedil;o</p>\r\n\r\n<p>Ar Cond,</p>\r\n\r\n<p>Piso Superior:</p>\r\n\r\n<p>Sala com home theater&nbsp;</p>\r\n\r\n<p>Churrasqueira Completa</p>\r\n\r\n<p>Piscina Privativa aquecida&nbsp;</p>\r\n\r\n<p>Ar. Cond.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '<p>Lazer completo.</p>\r\n\r\n<p>Rua sem sa&iacute;da.Conforto para visitantes.Seguran&ccedil;a.</p>\r\n\r\n<p>3 Torres.</p>\r\n', '<p>Localiza&ccedil;&atilde;o acesso a p&eacute; ao centro comercial da regi&atilde;o.</p>\r\n\r\n<p>Restaurantes Padaria Supermercados Academia</p>\r\n\r\n<p>F&aacute;cil acesso a marginal&nbsp;</p>\r\n', '<p><em>R$ 2.350.000,00</em></p>\r\n', '2016-07-09 00:58:47', '2016-07-13 21:11:38'),
(2, 2, 'inativo', 'Brás- São Paulo', 'bras-sao-paulo', 'Estação Brás ', 'screenhunter-21-jul-12-1336_20160712164008.jpg', '<p>Apartamentos de 53 a 127 m2&nbsp;</p>\r\n', '<p>Localiza&ccedil;&atilde;o estrat&eacute;gica para valoriza&ccedil;&atilde;o futura</p>\r\n', '<p>Rua da Alf&acirc;ndega&nbsp;</p>\r\n', '<p>A partir de 368.000</p>\r\n', '2016-07-09 01:00:15', '2016-07-12 19:40:08'),
(3, 3, 'inativo', 'Itaim Bibi - São Paulo', 'itaim-bibi-sao-paulo', 'One Eleven Home and Work ', 'screenhunter-18-jul-12-1313_20160712163413.jpg', '<p>Salas Comerciais de 32 m2 a 530 m2&nbsp;</p>\r\n', '<p>&Oacute;tima localiza&ccedil;&atilde;o.Mixed Use.Excelente Infraestrutura</p>\r\n', '<p>Rua Pequetita</p>\r\n', '<p>A partir de 576.000</p>\r\n', '2016-07-09 01:01:13', '2016-07-12 19:34:13'),
(4, 5, 'ativo', 'Pinheiros - São Paulo ', 'pinheiros-sao-paulo-1', 'Apartamento 1 por andar ', 'dsc-0294_20160713174516.jpg', '<p>Apartamento 250 m2 4 Su&iacute;tes 4VGS</p>\r\n\r\n<p>Fino acabamento.Ed&iacute;fico constru&ccedil;&atilde;o Moraes Sampaio,</p>\r\n', '<p>Fachada estilo classico.Amplo estacinamento para visitantes.</p>\r\n', '<p>Execelente localiza&ccedil;&atilde;o.Pr&oacute;ximo a restaurantes , padaria e supermercados.</p>\r\n\r\n<p>F&aacute;cil acesso a marginal , Av. Faria lima e esta&ccedil;&otilde;es de metro e trem.</p>\r\n', '<p><em><span style="color:#000000">R$ 2.900.000,00&nbsp;</span></em></p>\r\n', '2016-07-11 16:30:05', '2016-07-13 20:45:17'),
(5, 1, 'inativo', 'PINHEIROS, SÃO PAULO', 'pinheiros-sao-paulo', 'ACERVO PINHEIROS, SÃO PAULO', 'acervo-pinheiros_20160711151220.jpg', '<p>Pinheiros apartamentos tipo 332 m2 cobertura 581 m2&nbsp;</p>\r\n', '<p>Condom&iacute;nio alto padr&atilde;o.Lazer com quadra de tenis.</p>\r\n', '<p>Rua Sim&atilde;o Alvares - Pinheiros - SP</p>\r\n', '<p>A partir de 4.000.000,00</p>\r\n', '2016-07-11 16:32:22', '2016-07-11 18:12:21'),
(6, 4, 'ativo', 'Alto da Boa Vista - São Paulo ', 'alto-da-boa-vista-sao-paulo', 'Apartamento único em rua arborizada', 'foto-sb27_20160714153211.jpg', '<p>Apartamento 185 m2 3 Dormit&oacute;rios 2 Su&iacute;tes 3 VGS</p>\r\n\r\n<p>Excelente distribui&ccedil;&atilde;o de arm&aacute;rios de primeira qualidade.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '<p>2 Torres&nbsp;</p>\r\n\r\n<p>Elevador Privativo Hall Privativo</p>\r\n\r\n<p>Piscina adulto e infantil , Sal&atilde;o de Festas ,Brinquedoteca.</p>\r\n\r\n<p>Estacionamento para visitantes</p>\r\n\r\n<p>&nbsp;</p>\r\n', '<p>Localiza&ccedil;&atilde;o pr&oacute;ximo a padaria , supermercados , esta&ccedil;&atilde;o do metro.Pr&oacute;ximo a Shopping&nbsp;</p>\r\n\r\n<p>Acesso f&aacute;cil a marginal e Av. Adiolfo Pinheiros</p>\r\n', '<p><em>R$ 1.500.000,00</em></p>\r\n', '2016-07-14 18:29:02', '2016-07-14 18:35:12'),
(7, 7, 'ativo', 'Itaim Bibi - São Paulo ', 'itaim-bibi-sao-paulo-1', 'Apartamento 1 por andar Itaim Bibi na Planta ', 'lindenberg-itaim_20160718132059.jpg', '<p>Apartamento 1 por andar&nbsp;</p>\r\n\r\n<p>286 m2 com dep&oacute;sito 4 VGS&nbsp;</p>\r\n\r\n<p>Cobertura 521,4 m2 com dep&oacute;sito 5&nbsp;VGS</p>\r\n\r\n<p>Entrega em Janeiro/2019</p>\r\n\r\n<p>Pagamento facilitado !!</p>\r\n', '<p>Torre &uacute;nica&nbsp;</p>\r\n\r\n<p>24 andares&nbsp;</p>\r\n\r\n<p>Lazer Completo</p>\r\n\r\n<p>14 VGS para visitantes</p>\r\n\r\n<p>&nbsp;</p>\r\n', '<p>Rua Tabapu&atilde; - Itaim bibi</p>\r\n', '<p>Sob Consulta</p>\r\n', '2016-07-18 16:20:59', '2016-07-18 16:20:59');

-- --------------------------------------------------------

--
-- Estrutura da tabela `imoveis_imagens`
--

CREATE TABLE `imoveis_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `imovel_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `imoveis_imagens`
--

INSERT INTO `imoveis_imagens` (`id`, `imovel_id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(4, 2, 0, 'casa-02-7074m2_20160708220042.jpg', '2016-07-09 01:00:43', '2016-07-09 01:00:43'),
(9, 5, 0, 'acervo-pinheiros_20160711150747.jpg', '2016-07-11 18:07:48', '2016-07-11 18:07:48'),
(10, 5, 0, 'screenhunter-17-jul-11-1213_20160711151408.jpg', '2016-07-11 18:14:08', '2016-07-11 18:14:08'),
(11, 3, 0, 'screenhunter-18-jul-12-1313_20160712161547.jpg', '2016-07-12 19:15:47', '2016-07-12 19:15:47'),
(12, 3, 0, 'screenhunter-19-jul-12-1314_20160712163029.jpg', '2016-07-12 19:30:30', '2016-07-12 19:30:30'),
(13, 3, 0, 'screenhunter-20-jul-12-1314_20160712163057.jpg', '2016-07-12 19:30:57', '2016-07-12 19:30:57'),
(14, 4, 5, 'dsc-0132_20160713174643.jpg', '2016-07-13 20:46:44', '2016-07-13 20:46:44'),
(15, 4, 2, 'dsc-0111_20160713174707.jpg', '2016-07-13 20:47:08', '2016-07-13 20:47:08'),
(16, 4, 4, 'dsc-0123_20160713174726.jpg', '2016-07-13 20:47:27', '2016-07-13 20:47:27'),
(17, 4, 1, 'dsc-0105-2_20160713174731.jpg', '2016-07-13 20:47:32', '2016-07-13 20:47:32'),
(18, 4, 3, 'dsc-0113_20160713174741.jpg', '2016-07-13 20:47:42', '2016-07-13 20:47:42'),
(19, 4, 29, 'dsc-0117_20160713174823.jpg', '2016-07-13 20:48:24', '2016-07-13 20:48:24'),
(20, 4, 7, 'dsc-0137_20160713174823.jpg', '2016-07-13 20:48:24', '2016-07-13 20:48:24'),
(21, 4, 8, 'dsc-0151_20160713174852.jpg', '2016-07-13 20:48:53', '2016-07-13 20:48:53'),
(22, 4, 6, 'dsc-0134_20160713174910.jpg', '2016-07-13 20:49:11', '2016-07-13 20:49:11'),
(23, 4, 10, 'dsc-0162_20160713174936.jpg', '2016-07-13 20:49:37', '2016-07-13 20:49:37'),
(24, 4, 9, 'dsc-0157_20160713174942.jpg', '2016-07-13 20:49:43', '2016-07-13 20:49:43'),
(25, 4, 26, 'dsc-0146_20160713174951.jpg', '2016-07-13 20:49:55', '2016-07-13 20:49:55'),
(26, 4, 11, 'dsc-0180_20160713175020.jpg', '2016-07-13 20:50:21', '2016-07-13 20:50:21'),
(27, 4, 12, 'dsc-0185_20160713175037.jpg', '2016-07-13 20:50:38', '2016-07-13 20:50:38'),
(28, 4, 13, 'dsc-0194_20160713175047.jpg', '2016-07-13 20:50:48', '2016-07-13 20:50:48'),
(29, 4, 14, 'dsc-0204_20160713175101.jpg', '2016-07-13 20:51:03', '2016-07-13 20:51:03'),
(30, 4, 16, 'dsc-0216_20160713175115.jpg', '2016-07-13 20:51:16', '2016-07-13 20:51:16'),
(31, 4, 15, 'dsc-0208_20160713175119.jpg', '2016-07-13 20:51:20', '2016-07-13 20:51:20'),
(32, 4, 19, 'dsc-0233_20160713175141.jpg', '2016-07-13 20:51:42', '2016-07-13 20:51:42'),
(33, 4, 17, 'dsc-0218_20160713175212.jpg', '2016-07-13 20:52:13', '2016-07-13 20:52:13'),
(34, 4, 20, 'dsc-0252_20160713175225.jpg', '2016-07-13 20:52:26', '2016-07-13 20:52:26'),
(35, 4, 21, 'dsc-0263_20160713175235.jpg', '2016-07-13 20:52:36', '2016-07-13 20:52:36'),
(36, 4, 18, 'dsc-0229_20160713175238.jpg', '2016-07-13 20:52:39', '2016-07-13 20:52:39'),
(37, 4, 22, 'dsc-0271_20160713175309.jpg', '2016-07-13 20:53:10', '2016-07-13 20:53:10'),
(38, 4, 23, 'dsc-0286_20160713175318.jpg', '2016-07-13 20:53:19', '2016-07-13 20:53:19'),
(39, 4, 25, 'dsc-0314_20160713175353.jpg', '2016-07-13 20:53:54', '2016-07-13 20:53:54'),
(41, 4, 28, 'dsc-0326_20160713175356.jpg', '2016-07-13 20:53:57', '2016-07-13 20:53:57'),
(42, 4, 24, 'dsc-0300_20160713175436.jpg', '2016-07-13 20:54:37', '2016-07-13 20:54:37'),
(43, 4, 27, 'dsc-0318_20160713175437.jpg', '2016-07-13 20:54:39', '2016-07-13 20:54:39'),
(44, 1, 4, 'dsc-0022_20160713181308.jpg', '2016-07-13 21:13:08', '2016-07-13 21:13:08'),
(45, 1, 1, 'dsc-0005_20160713181328.jpg', '2016-07-13 21:13:29', '2016-07-13 21:13:29'),
(46, 1, 3, 'dsc-0021_20160713181346.jpg', '2016-07-13 21:13:47', '2016-07-13 21:13:47'),
(47, 1, 5, 'dsc-0031_20160713181358.jpg', '2016-07-13 21:13:59', '2016-07-13 21:13:59'),
(48, 1, 2, 'dsc-0018_20160713181405.jpg', '2016-07-13 21:14:06', '2016-07-13 21:14:06'),
(49, 1, 6, 'dsc-0038_20160713181414.jpg', '2016-07-13 21:14:15', '2016-07-13 21:14:15'),
(50, 1, 7, 'dsc-0041_20160713181439.jpg', '2016-07-13 21:14:41', '2016-07-13 21:14:41'),
(51, 1, 8, 'dsc-0046_20160713181455.jpg', '2016-07-13 21:14:56', '2016-07-13 21:14:56'),
(52, 1, 10, 'dsc-0048_20160713181503.jpg', '2016-07-13 21:15:03', '2016-07-13 21:15:03'),
(53, 1, 9, 'dsc-0047_20160713181503.jpg', '2016-07-13 21:15:04', '2016-07-13 21:15:04'),
(54, 1, 11, 'dsc-0054_20160713181510.jpg', '2016-07-13 21:15:11', '2016-07-13 21:15:11'),
(55, 1, 12, 'dsc-0057_20160713181523.jpg', '2016-07-13 21:15:24', '2016-07-13 21:15:24'),
(56, 1, 13, 'dsc-0060_20160713181536.jpg', '2016-07-13 21:15:36', '2016-07-13 21:15:36'),
(57, 1, 14, 'dsc-0065_20160713181608.jpg', '2016-07-13 21:16:09', '2016-07-13 21:16:09'),
(58, 1, 16, 'dsc-0072_20160713181623.jpg', '2016-07-13 21:16:24', '2016-07-13 21:16:24'),
(59, 1, 17, 'dsc-0076_20160713181626.jpg', '2016-07-13 21:16:27', '2016-07-13 21:16:27'),
(60, 1, 15, 'dsc-0067_20160713181636.jpg', '2016-07-13 21:16:37', '2016-07-13 21:16:37'),
(61, 1, 18, 'dsc-0077_20160713181645.jpg', '2016-07-13 21:16:46', '2016-07-13 21:16:46'),
(62, 1, 19, 'dsc-0082_20160713181652.jpg', '2016-07-13 21:16:53', '2016-07-13 21:16:53'),
(63, 1, 20, 'dsc-0086_20160713181700.jpg', '2016-07-13 21:17:01', '2016-07-13 21:17:01'),
(64, 1, 23, 'gopr0036_20160713181747.jpg', '2016-07-13 21:17:48', '2016-07-13 21:17:48'),
(65, 1, 27, 'dsc-0095_20160713181752.jpg', '2016-07-13 21:17:53', '2016-07-13 21:17:53'),
(66, 1, 21, 'dsc-0092_20160713181756.jpg', '2016-07-13 21:17:57', '2016-07-13 21:17:57'),
(67, 1, 29, 'dsc-0105_20160713181756.jpg', '2016-07-13 21:17:57', '2016-07-13 21:17:57'),
(68, 1, 24, 'gopr0064_20160713181820.jpg', '2016-07-13 21:18:20', '2016-07-13 21:18:20'),
(69, 1, 25, 'gopr0068_20160713181820.jpg', '2016-07-13 21:18:21', '2016-07-13 21:18:21'),
(70, 1, 22, 'dsc-0101_20160713181823.jpg', '2016-07-13 21:18:24', '2016-07-13 21:18:24'),
(71, 1, 28, 'gopr0156_20160713181829.jpg', '2016-07-13 21:18:30', '2016-07-13 21:18:30'),
(72, 1, 26, 'gopr0088_20160713181831.jpg', '2016-07-13 21:18:32', '2016-07-13 21:18:32'),
(73, 1, 30, 'dsc-0099_20160713181833.jpg', '2016-07-13 21:18:34', '2016-07-13 21:18:34'),
(74, 6, 29, 'foto-sb_20160714152927.jpg', '2016-07-14 18:29:27', '2016-07-14 18:29:27'),
(75, 6, 18, 'foto-sb3_20160714152928.jpg', '2016-07-14 18:29:28', '2016-07-14 18:29:28'),
(76, 6, 19, 'foto-sb4_20160714152928.jpg', '2016-07-14 18:29:28', '2016-07-14 18:29:28'),
(77, 6, 28, 'foto-sb2_20160714152928.jpg', '2016-07-14 18:29:29', '2016-07-14 18:29:29'),
(78, 6, 21, 'foto-sb1_20160714152928.jpg', '2016-07-14 18:29:29', '2016-07-14 18:29:29'),
(79, 6, 23, 'foto-sb2-2_20160714152930.jpg', '2016-07-14 18:29:31', '2016-07-14 18:29:31'),
(80, 6, 26, 'foto-sb9_20160714152932.jpg', '2016-07-14 18:29:32', '2016-07-14 18:29:32'),
(81, 6, 25, 'foto-sb8_20160714152933.jpg', '2016-07-14 18:29:33', '2016-07-14 18:29:33'),
(82, 6, 20, 'foto-sb5_20160714152934.jpg', '2016-07-14 18:29:34', '2016-07-14 18:29:34'),
(83, 6, 22, 'foto-sb6_20160714152935.jpg', '2016-07-14 18:29:35', '2016-07-14 18:29:35'),
(84, 6, 1, 'foto-sb10_20160714152935.jpg', '2016-07-14 18:29:35', '2016-07-14 18:29:35'),
(85, 6, 24, 'foto-sb7_20160714152935.jpg', '2016-07-14 18:29:36', '2016-07-14 18:29:36'),
(86, 6, 3, 'foto-sb12_20160714152936.jpg', '2016-07-14 18:29:36', '2016-07-14 18:29:36'),
(87, 6, 2, 'foto-sb11_20160714152937.jpg', '2016-07-14 18:29:37', '2016-07-14 18:29:37'),
(88, 6, 7, 'foto-sb16_20160714152937.jpg', '2016-07-14 18:29:38', '2016-07-14 18:29:38'),
(89, 6, 5, 'foto-sb14_20160714152938.jpg', '2016-07-14 18:29:38', '2016-07-14 18:29:38'),
(90, 6, 6, 'foto-sb15_20160714152938.jpg', '2016-07-14 18:29:38', '2016-07-14 18:29:38'),
(91, 6, 4, 'foto-sb13_20160714152939.jpg', '2016-07-14 18:29:40', '2016-07-14 18:29:40'),
(92, 6, 8, 'foto-sb17_20160714152941.jpg', '2016-07-14 18:29:41', '2016-07-14 18:29:41'),
(93, 6, 10, 'foto-sb19_20160714152941.jpg', '2016-07-14 18:29:41', '2016-07-14 18:29:41'),
(94, 6, 12, 'foto-sb21_20160714152941.jpg', '2016-07-14 18:29:41', '2016-07-14 18:29:41'),
(95, 6, 11, 'foto-sb20_20160714152942.jpg', '2016-07-14 18:29:42', '2016-07-14 18:29:42'),
(96, 6, 13, 'foto-sb22_20160714152942.jpg', '2016-07-14 18:29:42', '2016-07-14 18:29:42'),
(97, 6, 9, 'foto-sb18_20160714152943.jpg', '2016-07-14 18:29:43', '2016-07-14 18:29:43'),
(98, 6, 14, 'foto-sb23_20160714152944.jpg', '2016-07-14 18:29:44', '2016-07-14 18:29:44'),
(99, 6, 16, 'foto-sb25_20160714152948.jpg', '2016-07-14 18:29:48', '2016-07-14 18:29:48'),
(101, 6, 15, 'foto-sb24_20160714152949.jpg', '2016-07-14 18:29:49', '2016-07-14 18:29:49'),
(102, 6, 27, 'foto-sb27_20160714152956.jpg', '2016-07-14 18:29:57', '2016-07-14 18:29:57'),
(103, 7, 0, 'lindenberg-itaim_20160718132126.jpg', '2016-07-18 16:21:27', '2016-07-18 16:21:27'),
(104, 7, 0, 'planta-ldi-itaim_20160718132151.png', '2016-07-18 16:21:52', '2016-07-18 16:21:52');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2016_03_01_000000_create_contatos_recebidos_table', 1),
('2016_06_30_144415_create_banners_table', 1),
('2016_06_30_145831_create_depoimentos_table', 1),
('2016_06_30_154331_create_parceiros_table', 1),
('2016_06_30_165100_create_quem_somos_table', 1),
('2016_06_30_170540_create_equipe_table', 1),
('2016_07_04_175343_create_imoveis_table', 1),
('2016_07_06_182654_create_cadastros_imoveis_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `parceiros`
--

CREATE TABLE `parceiros` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `parceiros`
--

INSERT INTO `parceiros` (`id`, `ordem`, `nome`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 2, 'Cyrela', 'screenhunter-33-jul-14-1018_20160714132404.jpg.png', '2016-07-09 01:04:55', '2016-07-14 16:24:04'),
(2, 5, 'BKO', 'screenhunter-31-jul-14-1016_20160714132338.jpg.png', '2016-07-09 01:04:59', '2016-07-14 16:23:38'),
(3, 26, 'Archtech', 'screenhunter-30-jul-14-1014_20160714132232.jpg.png', '2016-07-09 01:05:03', '2016-07-14 16:22:57'),
(7, 1, 'Brookfield', 'screenhunter-32-jul-14-1017_20160714132432.jpg.png', '2016-07-14 16:24:32', '2016-07-14 16:24:32'),
(8, 6, 'Even', 'screenhunter-34-jul-14-1019_20160714132547.jpg.png', '2016-07-14 16:25:47', '2016-07-14 16:25:47'),
(9, 7, 'Lindenberg', 'screenhunter-35-jul-14-1021_20160714132621.jpg.png', '2016-07-14 16:26:21', '2016-07-14 16:26:21'),
(10, 25, 'Eztec', 'screenhunter-36-jul-14-1022_20160714132650.jpg.png', '2016-07-14 16:26:50', '2016-07-14 16:26:50'),
(11, 16, 'Setin', 'screenhunter-43-jul-14-1033_20160714133340.jpg.png', '2016-07-14 16:33:40', '2016-07-14 16:33:40'),
(12, 8, 'Stan', 'screenhunter-39-jul-14-1031_20160714133420.jpg.png', '2016-07-14 16:34:20', '2016-07-14 16:34:20'),
(13, 3, 'Esser', 'screenhunter-40-jul-14-1032_20160714133456.jpg.png', '2016-07-14 16:34:56', '2016-07-14 16:34:56'),
(14, 24, 'Yuny', 'screenhunter-44-jul-14-1036_20160714134052.jpg.png', '2016-07-14 16:40:52', '2016-07-14 16:40:52'),
(16, 11, 'Helbor', 'screenhunter-46-jul-14-1038_20160714134145.jpg.png', '2016-07-14 16:41:45', '2016-07-14 16:41:45'),
(17, 15, 'Odebrecht', 'screenhunter-47-jul-14-1040_20160714134217.jpg.png', '2016-07-14 16:42:17', '2016-07-14 16:42:17'),
(18, 23, 'Aurinova', 'screenhunter-50-jul-14-1426_20160714172810.jpg.png', '2016-07-14 20:28:10', '2016-07-14 20:28:10'),
(19, 22, 'GR Properties', 'screenhunter-49-jul-14-1425_20160714172836.jpg.png', '2016-07-14 20:28:36', '2016-07-14 20:28:36'),
(20, 21, 'Huma', 'screenhunter-48-jul-14-1423_20160714172858.jpg.png', '2016-07-14 20:28:58', '2016-07-14 20:28:58'),
(21, 20, 'Toledo Ferrari', 'screenhunter-51-jul-14-1437_20160714173824.jpg.png', '2016-07-14 20:38:24', '2016-07-14 20:38:24'),
(22, 19, 'Banco de Projetos', 'screenhunter-52-jul-14-1439_20160714173957.jpg.png', '2016-07-14 20:39:57', '2016-07-14 20:39:57'),
(23, 17, 'Bolsa de Imóveis ', 'screenhunter-53-jul-14-1458_20160714175923.jpg.png', '2016-07-14 20:59:23', '2016-07-14 20:59:23'),
(24, 28, 'Jones Lang Lasalle', 'screenhunter-54-jul-14-1501_20160714180131.jpg.png', '2016-07-14 21:01:31', '2016-07-14 21:01:31'),
(25, 12, 'Paulo Mauro', 'screenhunter-55-jul-14-1506_20160714180650.jpg.png', '2016-07-14 21:06:50', '2016-07-14 21:06:50'),
(27, 13, 'Lucio', 'screenhunter-57-jul-14-1513_20160714181343.jpg.png', '2016-07-14 21:13:43', '2016-07-14 21:13:43'),
(28, 18, 'Benx', 'screenhunter-58-jul-14-1515_20160714181603.jpg.png', '2016-07-14 21:16:03', '2016-07-14 21:16:03'),
(29, 27, 'You Inc', 'screenhunter-59-jul-14-1518_20160714181838.jpg.png', '2016-07-14 21:18:38', '2016-07-14 21:18:38'),
(30, 9, 'Tri Sul', 'screenhunter-61-jul-14-1711_20160714201407.jpg.png', '2016-07-14 23:14:08', '2016-07-14 23:14:08'),
(31, 4, 'Fraiha', 'screenhunter-60-jul-14-1708_20160714201440.jpg.png', '2016-07-14 23:14:40', '2016-07-14 23:14:40'),
(32, 10, 'Gafisa', 'screenhunter-62-jul-15-1800_20160715210158.jpg.png', '2016-07-16 00:01:58', '2016-07-16 00:01:58'),
(33, 14, 'Porte Engenharia', 'screenhunter-63-jul-15-1803_20160715210342.jpg.png', '2016-07-16 00:03:42', '2016-07-16 00:03:42');

-- --------------------------------------------------------

--
-- Estrutura da tabela `quem_somos`
--

CREATE TABLE `quem_somos` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_equipe` text COLLATE utf8_unicode_ci NOT NULL,
  `metodologia` text COLLATE utf8_unicode_ci NOT NULL,
  `etapa_1_titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `etapa_1_texto` text COLLATE utf8_unicode_ci NOT NULL,
  `etapa_2_titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `etapa_2_texto` text COLLATE utf8_unicode_ci NOT NULL,
  `etapa_3_titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `etapa_3_texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `quem_somos`
--

INSERT INTO `quem_somos` (`id`, `texto`, `texto_equipe`, `metodologia`, `etapa_1_titulo`, `etapa_1_texto`, `etapa_2_titulo`, `etapa_2_texto`, `etapa_3_titulo`, `etapa_3_texto`, `created_at`, `updated_at`) VALUES
(1, '<p>MAKLER REALTY AG&Ecirc;NCIA DE IM&Oacute;VEIS surge com o objetivo de cobrir uma lacuna no mercado imobili&aacute;rio, onde tem como principal intuito conseguir im&oacute;veis para seus clientes e n&atilde;o necessariamente clientes para seus im&oacute;veis.</p>\r\n\r\n<p>Para obtermos &ecirc;xito at&eacute; a finaliza&ccedil;&atilde;o desta tarefa contamos com corpo jur&iacute;dico e t&eacute;cnico na &aacute;rea imobili&aacute;ria, proporcionando uma rela&ccedil;&atilde;o comercial segura aos nossos clientes com atendimento preciso e eficaz.</p>\r\n\r\n<p class="quem-somos-titulo">Im&oacute;veis para clientes,</p>\r\n\r\n<p class="quem-somos-subtitulo">&nbsp;n&atilde;o clientes para im&oacute;veis</p>\r\n\r\n<p><strong>Quem Somos:</strong></p>\r\n\r\n<p>MAKL&Eacute;R REALTY &eacute; uma ideia que surgiu h&aacute; 5 anos, onde seu fundador Alexandre &ldquo;Makl&eacute;r&rdquo;, atuando no mercado imobili&aacute;rio como corretor de im&oacute;veis, procurava uma forma de oferecer um servi&ccedil;o diferenciado aos seus clientes em mercado bastante concorrido, sendo assim, convida 2 parceiros para compartilhar da sua ideia inovadora, Dr. Tadeu Frederico de Andrade com experi&ecirc;ncia de mais de 20 anos em direito imobili&aacute;rio e perito avaliador de im&oacute;veis e Luciana Krettlis Gon&ccedil;alves com experi&ecirc;ncia de mais de 10 anos como Arquiteta prestando servi&ccedil;os de reforma e/ ou constru&ccedil;&atilde;o, especializada em laudos periciais junto a Caixa Econ&ocirc;mica Federal e Banco do Brasil.</p>\r\n\r\n<p>E, com rela&ccedil;&atilde;o a produtos, trabalhamos em parceria com as principais incorporadoras do pa&iacute;s e, tamb&eacute;m, temos cadastrados em nossa carteira imobili&aacute;ria, diversos im&oacute;veis, residenciais, comerciais e outros.</p>\r\n\r\n<p>Gostar&iacute;amos muito que fizesse parte de nossa hist&oacute;ria, nos dando a oportunidade em t&ecirc;-lo como cliente, contando com nosso profissionalismo, atitude, respeito e comprometimento em nossas a&ccedil;&otilde;es.</p>\r\n', '<p>Texto equipe faltando.</p>\r\n', 'A Maklér Realty através de seu know how e experiência tem maior facilidade para encontrar a melhor opção para seus clientes.', 'Informações', 'Nesta etapa serão colhidos os detalhes do que o cliente busca , etapa essa de suma importância , pelo fato de algumas vezes o próprio cliente ainda não tem a exata definição da sua busca, assim nós daremos o suporte necessário para que isto ocorra.', 'Busca', 'Nesta etapa nossa equipe irá levantar as opções que se assemelham a busca do cliente, nossas buscas são únicas especificas para cada cliente.', 'Opções', 'Nesta etapa serão apresentadas ao cliente as opções disponíveis no mercado naquele momento.\r\n', NULL, '2016-07-14 18:36:32');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$BpycGXYXk4vkr3Pesv6M7.Z2K.KklzS/G1.5HNeXw/kOMp0mWTjw2', 'WUQOTfdPxtzow023OpZYAWSave8uT6lDlzIPVnjsTMIr5A4pUDGF41tVNjwK', NULL, '2016-07-11 16:37:20'),
(2, 'makler', 'contato@maklerrealty.com.br', '$2y$10$tSnHATkUKQSe9FBVGSuAbuKpNTnIB0umRRjUYk51khcvrL4/eztLa', 'TJoYymMvHjYeU3ukCjRk53XXhoTJ0krD9dJjZf5hXH1FZDZ5rmcSZPc98awa', '2016-07-11 16:37:12', '2016-07-18 16:23:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cadastros_imoveis`
--
ALTER TABLE `cadastros_imoveis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cadastros_imoveis_imagens`
--
ALTER TABLE `cadastros_imoveis_imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cadastros_imoveis_imagens_cadastro_id_foreign` (`cadastro_id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `depoimentos`
--
ALTER TABLE `depoimentos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `equipe`
--
ALTER TABLE `equipe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imoveis`
--
ALTER TABLE `imoveis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imoveis_imagens`
--
ALTER TABLE `imoveis_imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `imoveis_imagens_imovel_id_foreign` (`imovel_id`);

--
-- Indexes for table `parceiros`
--
ALTER TABLE `parceiros`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quem_somos`
--
ALTER TABLE `quem_somos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cadastros_imoveis`
--
ALTER TABLE `cadastros_imoveis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cadastros_imoveis_imagens`
--
ALTER TABLE `cadastros_imoveis_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `depoimentos`
--
ALTER TABLE `depoimentos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `equipe`
--
ALTER TABLE `equipe`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `imoveis`
--
ALTER TABLE `imoveis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `imoveis_imagens`
--
ALTER TABLE `imoveis_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
--
-- AUTO_INCREMENT for table `parceiros`
--
ALTER TABLE `parceiros`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `quem_somos`
--
ALTER TABLE `quem_somos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `cadastros_imoveis_imagens`
--
ALTER TABLE `cadastros_imoveis_imagens`
  ADD CONSTRAINT `cadastros_imoveis_imagens_cadastro_id_foreign` FOREIGN KEY (`cadastro_id`) REFERENCES `cadastros_imoveis` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `imoveis_imagens`
--
ALTER TABLE `imoveis_imagens`
  ADD CONSTRAINT `imoveis_imagens_imovel_id_foreign` FOREIGN KEY (`imovel_id`) REFERENCES `imoveis` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
